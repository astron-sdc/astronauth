""" ASTRONauth Django App """
from django.apps import AppConfig


class AstronauthConfig(AppConfig):
    """ASTRONauth Django App Configuration"""

    default_auto_field = "django.db.models.BigAutoField"
    name = "astronauth"
